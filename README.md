# README #

This README would normally document whatever steps are necessary to get your application up and running.

I use rabbit mq via docker container, run rabbit mq on docker below command on your system:

docker run -d --hostname my-rabbit --name myrabbit -p 5672:5672 -p 15672:15672 rabbitmq:3-management

you can pass environment variables like "-e  RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=admin" if you do not pass username and password will be "guest"

after you run rabbit mq on your system you can check on your browser "localhost:15672/" 

then run producer project and add request on "http://localhost:8081/rabbitmq/send" then you will see "Message sent to the RabbitMQ Successfully" message on browser

test your rabbit mq that message is on your queue.

Then start consumer project and see message will taken from queue.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact