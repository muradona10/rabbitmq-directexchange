package com.md.rabbitmqconsumer.service;

import com.md.rabbitmqconsumer.model.Company;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQConsumer {

    @RabbitListener(queues = "${md.queue}", containerFactory = "jsaFactory")
    public void consume(Company company){
        System.out.println(company.toString());
    }

}
