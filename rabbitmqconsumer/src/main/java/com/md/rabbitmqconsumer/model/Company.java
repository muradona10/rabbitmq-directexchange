package com.md.rabbitmqconsumer.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id", scope = Company.class)
public class Company {

    private String name;

    private List<Product> products;

    public Company(){}

    public Company(String name, List<Product> products){
        this.name=name;
        this.products=products;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {

        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("name", this.name);

            JSONArray productArray = new JSONArray();
            if(this.products != null) {
                this.products.forEach(product -> {
                    JSONObject subJson = new JSONObject();
                    try {
                        subJson.put("name", product.getName());
                    }catch (JSONException ex){
                        ex.printStackTrace();
                    }
                    productArray.put(subJson);
                });
            }
            jsonObject.put("products", productArray);

        } catch (JSONException ex){
            ex.printStackTrace();
        }

        return jsonObject.toString();
    }
}
