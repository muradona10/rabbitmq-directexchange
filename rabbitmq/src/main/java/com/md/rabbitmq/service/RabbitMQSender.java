package com.md.rabbitmq.service;


import com.md.rabbitmq.model.Company;
import com.md.rabbitmq.model.Employee;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${md.rabbitmq.exchange}")
    private String exchange;

    @Value("${md.rabbitmq.routingkey}")
    private String routingkey;

    public void produce(Company company){
        rabbitTemplate.convertAndSend(exchange, routingkey, company);
        System.out.println("Send msg = " + company);
    }


}
