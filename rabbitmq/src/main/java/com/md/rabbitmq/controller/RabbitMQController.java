package com.md.rabbitmq.controller;

import com.md.rabbitmq.model.Company;
import com.md.rabbitmq.model.Product;
import com.md.rabbitmq.service.RabbitMQSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/rabbitmq")
public class RabbitMQController {

    @Autowired
    RabbitMQSender rabbitMQSender;

    @GetMapping(value = "/send")
    public String producer() {

        Company samsung = new Company();
        samsung.setName("Samsung");
        List<Product> products = Arrays.asList(new Product("Galaxy S10",samsung),
                                               new Product("Note 10", samsung),
                                               new Product("Galaxy A3", samsung));
        samsung.setProducts(products);

        rabbitMQSender.produce(samsung);

        return "Message sent to the RabbitMQ Successfully";
    }

}
